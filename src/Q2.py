# -*- coding: utf-8 -*-
def loadData(fileName):  
    inFile = open(fileName, 'r')
    
    X = []  #declaring X and Y
    y = []  
  
    for line in inFile:  
        trainingSet = line.split(',') 
        X.append(trainingSet[0])   #we stock the first column of main in X, the 2nd one in Y
        y.append(trainingSet[1])   
  
    return (X, y)    

import pylab 
 
def plotData(X, y):  
    length = len(y)  #affecting y'th length
              
    pylab.figure(1)  #creating the first figure
  
    pylab.plot(X, y, 'rx') # what to plot and chmod  
    pylab.xlabel('z : 1 dimension coordinates ')  
    pylab.ylabel('PSI : Solution of the equation ')  
  
    pylab.show() 

(X,y) = loadData('Q2.txt')  # loading Q2 which contains Z and PSI 
  
plotData(X,y)  
