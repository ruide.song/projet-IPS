#ifndef ORTHO_H
#define ORTHO_H

#include <fstream>
#include <iostream>
#include <armadillo>
#include "Hermite.h"
#include "CD_HO.h"

using namespace std;

class ortho
{
	public :
	  double taille;
	  double n;
	  double pi;
	  arma::mat sigma;
	  arma::colvec xi;
	  arma::colvec wi;
	  
	  Hermite h;
	  
	  ortho(double taille, double n);
	  void set();
	  double f(double n);
	  double cal_c(double m, double n);
	  void cal_sigma(double i, double j);
	  void allsigma();
	
	
};
#endif
