# -*- coding: utf-8 -*-
def loadData(fileName):  
    inFile = open(fileName, 'r')
    
    X = []  #declaring X and Y
    y = []  
  
    for line in inFile:  
        trainingSet = line.split(',') 
        X.append(trainingSet[0])   #we stock the first column of main in X, the 2nd one in Y
        y.append(trainingSet[1])   
  
    return (X, y)    

import pylab 

def plotData(X, y):  
    length = len(y)  
              
    pylab.figure(1)  
  
    pylab.plot(X, y, 'rx')  
    pylab.xlabel('z : 1 dimension coordinates ')  
    pylab.ylabel('associated energy ')  
  
    pylab.show() 

(X,y) = loadData('Q3.txt')  
  
plotData(X,y)  
