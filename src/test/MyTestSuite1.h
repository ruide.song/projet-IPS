//Mytestsuite1.h
#include <cxxtest/TestSuite.h>
#include <iostream>
#include <fstream>
#include <armadillo>
#include "../Hermite.h"
#include "../CD_HO.h"
#include "../ortho.h"

using namespace std;

class  MyTestSuite1 : public CxxTest::TestSuite // le nom de la classe doit etre celui du .h courant
{
 public:
  void testHermite_recursif(void)
  {
     double   nhermite=3;
    
    /* class instanciation following */

    Hermite hermite_instance =  Hermite();
   

    
    double herm1=hermite_instance.Hermite_poly(nhermite,10);
    double herm2=hermite_instance.Hermite_poly(nhermite+1,10);
    double herm3=hermite_instance.Hermite_poly(nhermite+2,10);
    TS_TRACE("debut des test hermite");
    //  TS_ASSERT( 1 + 1 > 1 ); // verifie si l'expression est vraie ou pas
    TS_ASSERT_EQUALS( herm3, 2*10*herm2 - (2*(nhermite+1))*herm1 );  // verifie si l'expression est vraie ou pas	
    TS_TRACE("fin des test hermite");

  }
  
  void test_energy (void)
  {
    TS_TRACE("debut des test d'energie");
    double  n,i,l;
      n=0;
    i = 10000;
    l=1;
    
    CD_HO test_instance =  CD_HO(i,n);

    test_instance.auto_fill_Z(l); // pour z
    test_instance.fill_H(); // pour z
    test_instance.solution(); 

    arma::colvec energy = test_instance.energy();  
    double energy_formula1;
    energy_formula1 = (n+0.5)*test_instance.h*test_instance.w;
    
    TS_ASSERT_DELTA(energy_formula1,energy(10),0.01); 
    /* example of failure TS_ASSERT_DELTA(2,10,0.01); */
    TS_ASSERT_DELTA(energy_formula1,energy(100),0.01);
    TS_ASSERT_DELTA(energy_formula1,energy(1000),0.01); 	
    
 
    TS_TRACE("fin des test d'energie");
    
  }


  
};
