#include <iostream>
#include <armadillo>
#include <math.h>
#include "Hermite.h"
#include "CD_HO.h"
#define _USE_MATH_DEFINES

/**
 * @file CD_HO.cpp
 * @brief this file is meant to implement the calculation of the 1D-HO solutions of the equation
 * @author Ruide_SONG abdourahmane afifi
 * @date 12 November 2017
 */


using namespace std;

/**  
* This function is a constructor for our CD_HO class 
*  m, w, h and pi are cofficients. n is the degree
*  Z is a vector of input, H is a vector of Hermite, F is a vector of results also know as PSI 
*@return nothing, the class is initialized though 
*/

	CD_HO::CD_HO(double i,double n){
	  this->n= n;
	  taille=i;
	  pi=M_PI;
	  m=1;
	  w=1; 
	  h=1;
    	  
	  Z.reshape(i,1);
	  H.reshape(i,1);
	  F.reshape(i,1);
	}
	
/**  
* This function calculates the factorial for degree n
* @return results of the factorial
*/

	int CD_HO::factorielle()
	{
	  int i,x=1;
	  for(i=1;i<=n;i++)
	    x=x*i;
	  return x;
	}

/**  
* This function is made to fill the Z vector. User has to fill in the fields by himself
*/

	void CD_HO::fill_Z(){
	  int j;
	  
	  
	  for(j=0;j<taille;j++){
		cout<< "Entrez z [" << j << "] \n";
		cin>>Z(j);
	
	  }
	  Z.print("z = ");
	}


/**  
*  This function is made to fill the Z vector.
* field values will be automatically generated.
* @param i The scope of vector Z.(from -i to i), called "limite" in french
*  
*/
	void CD_HO::auto_fill_Z(double i){
	  int j;
	  double step = 2*i/taille; 
	  for(j=0;j<taille;j++){
		Z(j)=-i+j*step; 
	  }
	}

/**  
* This function can caluelate the  Hermite of vector. We use recurrence relation
*
* @return H, which is the Hermite-polynom associated to our Z vector 
*/
	arma::colvec CD_HO::fill_H(){
	  int j;
	  for(j=0;j<taille;j++){
		H(j)=h1.Hermite_poly(n,Z(j)*sqrt(m*w/h));
	  } 
	  return H;
	}
	

/**  
* This function computes the 1D-HO solutions
*
* @return F, in other words the PHI vector, which is analytic 1D-HO solution
*/
	arma::colvec CD_HO::solution(){
	  int j;
	  for(j=0;j<taille;j++){
		F(j)=1/sqrt(factorielle()*pow(2,n)) 
		     *pow(m*w/(pi*h),0.25) 
		     * exp(-(m*w*pow(Z(j),2))/(2*h)) * H(j);
	  } 
	  return F;
	}

/**  
* This function computes the derivated value for a vector
* Let's say you want to derivate a function value(a g-vector) regarding another one.
* you will get the sensitivity to change of the function value g with respect to a change (time, position, whatever..)
* @return res
*/

	arma::colvec CD_HO::derivate(arma::colvec a_deriver, arma::colvec dx)
	  {
  	/* df/dx = f(a) -f(b) / a-b */
  
  	  arma::colvec shifted_n = arma::shift(a_deriver, -1) ;
 	  arma::colvec difference_n = a_deriver - shifted_n ;
  	  arma::colvec shifted_d = arma::shift(dx,-1);
	  arma::colvec difference_d = dx- shifted_d;
  
  	  arma::colvec res =  difference_n/difference_d;
  
 	  return res ; 
	}

/**
 * calculates the energy, referring to the definition stated at :/cours-IPS/dubrayn.github.io/IPS-DEV-TP/index.html#5
 *We won't exploit the last 2 elements of the vector because of the derivation problem
 */	arma::colvec CD_HO::energy()
	{
  	  arma::colvec terme1=((h*h)/(2*m))*derivate((derivate(F,Z)),Z);
    	  arma::colvec terme2 = 0.5*m*w*w*(Z%Z); /* Z%Z is the result of element by element multiplication of Z by itself*/
  	  arma::colvec energy = -terme1/F+terme2;	  
	  // energy.print("E =");
    	  return energy;
	}


	
