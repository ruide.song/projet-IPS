#include <iostream>
#include <armadillo>
#include "Hermite.h"

/**
 * @file Hermite.cpp
 * @brief this cpp can caluelate the Hermite
 * @author Ruide_SONG    abdourahmane_afifi
 * @date 12 November 2017
 */

using namespace std;


/**  
* This function is for initialization
*  
*/
	Hermite::Hermite(){
	  hn=1;		
	}


/**  
* This function can caluelate the nth-order Hermite polynomial
* @param n The nth-order Hermite polynomial is a polynomial of degree n
* @param z input for Hermite 
* @return hn result of the nth-order Hermite polynomial when input is z
*/
	double Hermite:: Hermite_poly(double n,double z)
	{ 
	  if(n==0) 
	     hn=1;  
	  else if(n==1)
  	     hn=2*z;
	  else
	     hn=2*z* Hermite_poly(n-1,z)-2*(n-1)* Hermite_poly(n-2,z);	
	  return hn; 
	}
	

