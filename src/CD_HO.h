#ifndef CD_HO_H
#define CD_HO_H

#include<iostream>
#include <armadillo>
#include "Hermite.h"
#include "CD_HO.h"
using namespace std;

class CD_HO
{
	public :
	  double taille;
	  double n;
   	  double pi;
	  double m;
	  double w; 
	  double h;
	  
	  arma::colvec Z;
	  arma::colvec H;
	  arma::colvec F;
	  
	  Hermite h1;
	  CD_HO();
	CD_HO(double i,double n);
	int factorielle();	
	void fill_Z();
	void auto_fill_Z(double i);
	arma::colvec fill_H();
	arma::colvec solution();

	arma::colvec derivate(arma::colvec a_deriver, arma::colvec dx);
	arma::colvec energy();
	
};
#endif


	
	
	
