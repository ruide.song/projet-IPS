#include <iostream>
#include <fstream>
#include <armadillo>
#include "Hermite.h"
#include "CD_HO.h"
#include "ortho.h"

using namespace std;
void python_Q2(CD_HO a);
void python_Q3(CD_HO a);

int main( )
{	
  /* We are starting Question 1 here : calculation of the 1D-HO solutions along z axis */
	double n,i,l;
	cout<< "choisir la taille du vecteur Z (nombre d'éléments de Z)\n";
	cin>>i;
	cout<< "choisir la limite(majorant) du vecteur Z (Si limite=1 , éléments de -1 à 1)\n";
	cin>>l;
	cout<<"entrez n (niveau d'energie?)  \n";
	cin>>n;
	CD_HO a(i,n) ;/* using the fiinstructor */
	a.auto_fill_Z(l);
	a.fill_H();
	a.solution();
	
     	/* We are starting Question 2 here : plotting the calculation of the 1D-HO solutions  */

	python_Q2(a);
	/* We are starting Question 3 here : verifying the energy associated to our equation */

	a.energy();	
	python_Q3(a);
	
	/*  We are starting Question 4 here : checking the orthonormality of the energy */
   	system("python ortho.py");
	double taille,m;
	cout<< "Q4: choisir la dimension de la matrice sigma ((par example limit=2 , n,m= 0 a 2))\n";
	cin>>m;
	taille=20; /*Here (as in ortho.py we chose an arbitrary n ( top-index of the quadratic sum)  dans Q4 */ 
	ortho s(taille,m); /* initialize */
	s.set();
	s.allsigma();
	s.sigma.print("sigma est");
	return 0;
}



void python_Q2(CD_HO a){
	int j ;
	ofstream outfile("Q2.txt");
	for(j=0;j<a.taille;j++ ){
	outfile << a.Z(j)<<','<< a.F(j)<< endl;
	}
	outfile.close();
	system("python Q2.py");
}


void python_Q3(CD_HO a){
	int j ;
	arma::colvec e=a.energy();
	ofstream outfile("Q3.txt");
	for(j=0;j<(a.taille)-2;j++ ){
	outfile << a.Z(j)<<','<< e(j) << endl;
	}
	outfile.close();
	system("python Q3.py");
}




