all: src doc test run_test
src:
	$(MAKE) -C src
launch:
	cd src/ && $(MAKE) launch
doc:
	$(MAKE) -C doc
test:
	$(MAKE) -C ./src/test
run_test:
	cd src/test/ && $(MAKE) run_test
.PHONY: clean src doc test
clean:
	$(MAKE) -C src clean
	$(MAKE) -C doc clean
	cd src/test/ && $(MAKE) clean
