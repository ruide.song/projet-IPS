# Groupe n°7


## Group


| Family name             | First name               | Email                   |
| -------------           |-------------             |-------------            |
| *SONG*                  | *Ruide*                  | *nsly2022335@gmail.com* |
| *Abdou*                 | *Afifi*                  |*abdourahmane.afifi@ensiie.fr*                         |

### Sujet : 1D Quantum Harmonic Oscillator
solutions to the 1D quantum harmonic oscillator will be calculated and some of their properties will be checked.(计算1D量子谐振子的解，并检查相应属性)<br>

**To launch the project:**
* type make all in root folder.
* type make launch to launch the program

### Adresse du backlog

*https://dubrayn.github.io/IPS-DEV/project.html*



